#!/bin/sh
make mega2560 && avrdude -P /dev/ttyACM0 -b 19200 -c avrisp -p atmega2560 -v -e -U efuse:w:0xFD:m -U hfuse:w:0xD8:m -U lfuse:w:0xFF:m -U flash:w:stk500boot_v2_mega2560.hex -U lock:w:0x0F:m
