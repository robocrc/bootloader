with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [ arduino avrdude ];
  shellHook =
  ''
  export PATH=$PATH:${arduino}/share/arduino/hardware/tools/avr/bin/
  '';
}

